import { combineReducers } from "redux";
import counter from "./counter/counter.reducer";
import user from "./user/user.reducer";

export default combineReducers({
  counter,
  user,
});
