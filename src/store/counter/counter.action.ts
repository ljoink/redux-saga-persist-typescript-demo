export enum EActionType {
  INCREMENTED = "counter/incremented",
  DECREMENTED = "counter/decremented",
  SET_HUNDRED = "counter/setHundred",
}

export type TIncremented = ReturnType<typeof incremented>;
const incremented = () => ({
  type: EActionType.INCREMENTED,
});

export type TDecremented = ReturnType<typeof decremented>;
const decremented = () => ({
  type: EActionType.DECREMENTED,
});

export type TSetHundred = ReturnType<typeof setHundred>;
const setHundred = () => ({
  type: EActionType.SET_HUNDRED,
  payload: 100,
});

export type TCounterAction = TIncremented | TDecremented | TSetHundred;

const counterAction = { incremented, decremented, setHundred };
export default counterAction;
