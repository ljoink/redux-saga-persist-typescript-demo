import { EActionType, TCounterAction, TSetHundred } from "./counter.action";
import initialState from "./counter.state";

export default function reducer(
  state = initialState,
  action: TCounterAction
): typeof initialState {
  switch (action.type) {
    case EActionType.INCREMENTED:
      return { value: state.value + 1 };
    case EActionType.DECREMENTED:
      return { value: state.value - 1 };
    case EActionType.SET_HUNDRED:
      const value = (action as TSetHundred).payload;
      return { value };
    default:
      return state;
  }
}
