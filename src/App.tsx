import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { RootState, AppDispatch } from "./store";
import { useDispatch, useSelector } from "react-redux";
import counterAction from "./store/counter/counter.action";
import userAction from "./store/user/user.action";

function App() {
  const counter = useSelector((state: RootState) => state.counter);
  const user = useSelector((state: RootState) => state.user);
  const dispatch: AppDispatch = useDispatch();

  const handleIncrement = () => {
    dispatch(counterAction.incremented());
  };

  const handleDecremented = () => {
    dispatch(counterAction.decremented());
  };

  const handleHundred = () => {
    dispatch(counterAction.setHundred());
  };

  const handleSignIn = () => {
    dispatch(userAction.apiSignIn());
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Counter: {counter.value}</p>
        <p>User.token: {user.token}</p>

        <button onClick={handleIncrement}>Increment</button>
        <br />
        <button onClick={handleDecremented}>Decrement</button>
        <br />
        <button onClick={handleHundred}>Hundred</button>
        <br />
        <button onClick={handleSignIn}>Sign In</button>
      </header>
    </div>
  );
}

export default App;
